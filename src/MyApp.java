import java.util.Scanner;

import javax.swing.text.TabExpander;
import javax.swing.text.html.StyleSheet;

public class MyApp {
    static void printWelcome() {
        System.out.println("Welcome to my app!!!");
    }

    static void printMenu() {
        System.out.println("--Menu--");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }

    static int inputChoice() {
        int choice;
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input your choice(1-3): ");
            choice = sc.nextInt();
            if (choice >= 1 && choice <= 3) {
                return choice;
            }
            System.out.println("Error: PLease input between 1-3");
        }
    }

    public static void main(String[] args) {
        int choice = 0;
        while (true) {
            printWelcome();
            printMenu();
            choice = inputChoice();
            process(choice);
        }
    }

    private static void process(int choice) {
        switch (choice) {
            case 1:
                printHelloWorld();
                break;
            case 2:
                addTwoNumber();

                break;
            case 3:
                exitProgram();
                break;
        }
    }

    static int ad(int first, int second) {
        int result = first + second;
        return result;
    }

    static void addTwoNumber() {
        Scanner sc = new Scanner(System.in);
        int first, second;
        int result;
        System.out.print("Please input first: ");
        first = sc.nextInt();
        System.out.print("Please input second: ");
        second = sc.nextInt();
        result = first + second;
        System.out.println("Result = " + result);
    }

    static int inputTime() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input time: ");
        int time = sc.nextInt();
        return time;
    }

    static void printHelloWorld() {
        int time = inputTime();
        for (int i = 0; i < time; i++) {
            System.out.println("Hello World!!!");
        }

    }

    static void exitProgram() {
        System.exit(0);
        System.out.println("Bye!!!");
    }
}
